print("container start")
try:
    import unzip_requirements
except ImportError:
    pass
print("unzip end")

import json
import numpy as np
import tflite_runtime.interpreter as tflite
import logging
import os
import tempfile
from contextlib import contextmanager
from pathlib import Path
import boto3
import botocore
import cv2
import dlib
import zipfile

logger = logging.getLogger("emotion_recognition")
labels = ["neutral", "happiness", "surprise", "sadness", "anger"]

MEDIA_BUCKET_NAME = "emsyte-media-production"
MODEL_BUCKET_NAME = "emr-model"
MODEL_FILE_NAME_KEY = "2020-10-01_0.73_converted_model.tflite"
# EMO_MODEL_FILE_NAME_KEY = 'saved_model.h5'
# GENDER_MODEL_FILE_NAME_KEY = 'saved_model.h5'
# AGE_MODEL_FILE_NAME_KEY = 'saved_model.h5'

OFFLINE = "IS_OFFLINE" in os.environ

if OFFLINE:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)


s3 = boto3.client("s3")


def get_model():
    if OFFLINE:
        return "/home/kevin/Documents/emo_rec/models/saved_model/"

    download_path = os.path.join("/tmp/", MODEL_FILE_NAME_KEY)

    # Download the model file if it's not already downloaded
    if not os.path.exists(download_path):
        print("downloading model...")

        os.makedirs(os.path.dirname(download_path), exist_ok=True)

        with open(download_path, "wb") as file:
            s3.download_fileobj(MODEL_BUCKET_NAME, MODEL_FILE_NAME_KEY, file)

    return download_path


def get_interpreter():
    model_path = get_model()

    interpreter = tflite.Interpreter(model_path=model_path)
    interpreter.allocate_tensors()

    return interpreter


interpreter = get_interpreter()


def classify(event, context):
    if OFFLINE:
        event = json.loads(event["body"])

    image = event["image"]

    logger.info("Processing image %s", image)

    # Downloading image from s3 and model
    with get_image(url=image) as image:
        # preprocess image
        image = cv2.imread(image.name)
        image = preProcess(image)
        print("loading model...")

        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        interpreter.set_tensor(input_details[0]["index"], image)
        interpreter.invoke()
        print("model loaded\n")
        # The function `get_tensor()` returns a copy of the tensor data.
        # Use `tensor()` in order to get a pointer to the tensor.
        pred = interpreter.get_tensor(output_details[0]["index"])
        # pred = np.argmax(pred)

        return {
            "success": True,
            "data": {"emotion": dict(zip(labels, pred[0].tolist()))},
        }


image_size = (64, 64)


def preProcess(image):
    """
    Grayscales, normalizes
    """
    coords, image = detectFace(image)
    image = np.array(image)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = np.array(cv2.resize(image, image_size))
    image = image.reshape((1, 64, 64, 1))
    image = image.astype("float32")
    image /= 255

    return image


faceDetector = dlib.get_frontal_face_detector()


def detectFace(image):
    """
    Detects face(s) in image and crops them out.
    """

    # print ("CurrentFace:", self.currentFaceDetectionFrequency)
    # faceDetector = dlib.get_frontal_face_detector()
    dets = faceDetector(image, 1)
    face = []

    for k, d in enumerate(dets):
        face = image[d.top() : d.bottom(), d.left() : d.right()]
        break

    return dets, face


@contextmanager
def get_image(url):
    if OFFLINE:
        yield open(url)
        return

    with download_file(url, MEDIA_BUCKET_NAME) as file:
        yield file


@contextmanager
def download_file(key, bucket):
    with tempfile.NamedTemporaryFile() as file:
        s3.download_fileobj(bucket, key, file)
        file.seek(0)
        yield file
